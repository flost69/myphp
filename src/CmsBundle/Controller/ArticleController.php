<?php

namespace CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use CmsBundle\Entity\Article;
use CmsBundle\Entity\Comment;
use CmsBundle\Form\CommentForm;

use CmsBundle\Form\ArticleForm;

class ArticleController extends Controller
{
    /**
     * @Route("/article/{id}", name="cms_article_show")
     * @Template("CmsBundle:Article:show.html.twig")
     */
    public function showAction($id) {
        $comment = new Comment();
        $em = $this->getDoctrine()->GetEntityManager();

        $article = $em->getRepository('CmsBundle:Article')->find($id);
        $form = $this->createForm(CommentForm::class, $comment);

        if(!$article) {
            throw $this->createNotFoundException('Nie znaleziono artykułu');
        }

        return array(
            'article' => $article,
            'form' => $form->createView()
        );
    }
    /**
     * @Route("/", name="cms_start")
     * @Template("CmsBundle:Article:index.html.twig")
     */
    public function indexAction() {
        return array();
    }
     /**
     * @Route("/create/article", name="cms_article_create")
     * @Method("GET")
     * @Template("CmsBundle:Article:create.html.twig")
     */
    public function createAction(Request $request) {

        $article = new Article();
        $form = $this->createForm(ArticleForm::class, $article);


        return $this->returnArticleForm($article, $form, array());
    }
    /**
     * @Route("/create/article", name="cms_article_create_post")
     * @Method("POST")
     * @Template("CmsBundle:Article:create.html.twig")
     */
    public function createPostAction(Request $request) {

        $article = new Article();
        $form = $this->createForm(ArticleForm::class, $article);

        $form->handleRequest($request);
        $validator = $this->get('validator');
        $errors = $validator->validate($article);

        if (count($errors) > 0) {
            return $this->returnArticleForm($article, $form, $errors);
        }

        $article->setCreated(new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->flush();

        return new RedirectResponse($this->generateUrl('cms_article_list'));

    }

    private function returnArticleForm($article, $form, $errors) {
        return $this->render('CmsBundle:Article:create.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
            'errors' => $errors
        ]);
    }
    /**
     * @Route("/list", name="cms_article_list")
     * @Template("CmsBundle:Article:list.html.twig")
     */
    public function listAction() {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('CmsBundle:Article')->findAll();

        return array(
            'articles' => $articles
        );
        
    }
    /**
    * @Route("/remove/{id}", name="cms_article_remove")
    */
    public function removeAction($id) {
        $em = $this->getDoctrine()->GetManager();

        $article = $em->getRepository('CmsBundle:Article')->find($id);
        
        $em->remove($article);
        $em->flush();

        return new RedirectResponse($this->generateUrl('cms_article_list'));
    }
}
