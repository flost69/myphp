<?php

namespace CmsBundle\Controller;

use CmsBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use CmsBundle\Entity\Comment;
use CmsBundle\Form\CommentForm;;

use CmsBundle\Form\ArticleForm;

class CommentController extends Controller
{
    /**
     * @Route("/create/comment", name="cms_comment_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $id = $request->query->get('id');

        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository('CmsBundle:Article')->find($id);
        $comment = new Comment();

        $form = $this->createForm(CommentForm::class, $comment);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $validator->validate($comment);

        if (count($errors) > 0) {
            return $this->render('CmsBundle:Article:show.html.twig', [
                'article' => $article,
                'form' => $form->createView()
            ]);
        }

        $comment->setCreated(new \DateTime());
        $comment->setArticle($article);

        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();

        return new RedirectResponse($this->generateUrl('cms_article_show', array(
            'id' => $id
        )));
    }
}